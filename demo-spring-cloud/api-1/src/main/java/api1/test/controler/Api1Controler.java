/**
 *
 */
package api1.test.controler;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author NV-Dev
 *
 */
@RestController
@RefreshScope
@ConfigurationProperties("test")
public class Api1Controler {

//    @Value("${test.property}")
    private String property;
//    @Value("${test.local.property}")
//    private String localProperty;

    @GetMapping(value = "/demo")
    public String demo() {
        return property ;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

//    public String getLocalProperty() {
//        return localProperty;
//    }
//
//    public void setLocalProperty(String localProperty) {
//        this.localProperty = localProperty;
//    }

}
