/**
 * 
 */
package apigateway.nv.apigateway;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

/**
 * @author NV-Dev
 *
 */
@Component
@Primary
@EnableAutoConfiguration
public class DocumentSwagger implements SwaggerResourcesProvider {
	@Override
	public List<SwaggerResource> get() {
		List<SwaggerResource> resources = new ArrayList<>();
		resources.add(swaggerResource("client1" ,"/spring-cloud-config-client", "2.0"));
		resources.add(swaggerResource("client2" ,"/api-2", "2.0"));
		//resources.add(swaggerResource("api-3" ,"/v2/api-docs", "2.0"));
		/*Map<String, ZuulRoute> routers = properties.getRoutes();
		routers.keySet().stream().forEach(route ->{
			resources.add(swaggerResource(route,routers.get(route).getLocation(), "2.0"));
		});*/
		return resources;
	}
	
	private SwaggerResource swaggerResource(String name, String location, String version){
		SwaggerResource resource = new SwaggerResource();
		resource.setName(name);
		resource.setLocation(location);
		resource.setSwaggerVersion(version);
		return resource;
	}
}