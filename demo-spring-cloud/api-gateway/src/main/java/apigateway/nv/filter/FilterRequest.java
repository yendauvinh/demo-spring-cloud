/**
 * 
 */
package apigateway.nv.filter;

import com.netflix.zuul.ZuulFilter;

import java.nio.charset.Charset;
import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.netflix.zuul.context.RequestContext;

import apigateway.nv.services.AdminServices;

/**
 * @author NV-Dev
 *
 */

public class FilterRequest extends ZuulFilter{
	private static final Logger LOG = LoggerFactory.getLogger(FilterRequest.class);
	
	@Autowired
	private AdminServices adminService; 
	
	@Override
	public String filterType() {
		return "pre";
	}
	
	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run()  {
		if(!validate()){
			RequestContext ctx = RequestContext.getCurrentContext();
			ctx.setResponseBody("{\"code\": \"500\",\"message\": \"\",\"data\": \"Account doesn't valid\",\"isSuccess\" : false}");
            ctx.getResponse().setContentType("application/json");
            ctx.getResponse().setStatus(500);
            ctx.setSendZuulResponse(false);
            return "/err";
		}
	    return null;
	}
	
	private Boolean validate(){
		RequestContext ctx = RequestContext.getCurrentContext();
	    HttpServletRequest request = ctx.getRequest();
	    String authorization = request.getHeader("Authorization");
	    if (authorization != null && authorization.startsWith("Basic")) {
	    	// Authorization: Basic base64credentials
	        String base64Credentials = authorization.substring("Basic".length()).trim();
	        String credentials = new String(Base64.getDecoder().decode(base64Credentials),
	                Charset.forName("UTF-8"));
	        // credentials = username:password
	        final String[] values = credentials.split(":",2);
	        return adminService.validationUserAndPass(values[0], values[1]);
	    }else return false;
	}
}
