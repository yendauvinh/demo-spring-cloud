/**
 * 
 */
package apigateway.nv.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import apigateway.nv.entity.Admin;

/**
 * @author NV-Dev
 *
 */
@Repository
public interface AdminRepository extends  JpaRepository<Admin, Long>{
	@Transactional
	@Query("SELECT o FROM Admin o WHERE o.userName = ?1 ")
	public Admin findByUserName(String userName);
}
