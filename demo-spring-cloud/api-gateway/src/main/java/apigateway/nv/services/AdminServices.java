/**
 * 
 */
package apigateway.nv.services;

import apigateway.nv.entity.Admin;

/**
 * @author NV-Dev
 *
 */
public interface AdminServices {
	Boolean validationUserAndPass(String userName, String passWord);
}
