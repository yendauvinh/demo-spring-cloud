/**
 * 
 */
package apigateway.nv.servicesimpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import apigateway.nv.entity.Admin;
import apigateway.nv.repository.AdminRepository;
import apigateway.nv.services.AdminServices;

/**
 * @author NV-Dev
 *
 */
@Service
public class AdminServicesImpl implements AdminServices {
	@Autowired
	private AdminRepository adminRipo;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public Boolean validationUserAndPass(String userName, String passWord) {
		Admin crrAdmin = adminRipo.findByUserName(userName);
		if(crrAdmin == null) return false;
		else return bCryptPasswordEncoder.matches(passWord, crrAdmin.getPassword());
	}

}
