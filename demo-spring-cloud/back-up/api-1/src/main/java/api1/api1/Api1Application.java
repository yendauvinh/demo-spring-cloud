package api1.api1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan( "api1.controler")
//@PropertySource("classpath:application.properties")
public class Api1Application {

	public static void main(String[] args) {
		System.out.println("-------------api-1-----------------------");
		SpringApplication.run(Api1Application.class, args);
	}
}
