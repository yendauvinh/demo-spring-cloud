/**
 * 
 */
package api1.controler;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author NV-Dev
 *
 */
@RestController
@RefreshScope
@Configuration
public class Api1Controler {
	@Value("${test.property}")
    private String testProperty;

    @Value("${test.local.property}")
    private String localTestProperty;
    
	@RequestMapping("/")
	public String demo(@RequestParam("data") String data){
		System.out.println(data);
		return "service 1 " + testProperty + " = " + localTestProperty;
	}
}
