/**
 * 
 */
package api2.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author NV-Dev
 *
 */
@RestController
public class Api2Controller {
	@RequestMapping("/2")
	public String demo(@RequestParam("data") String data){
		System.out.println(data);	
		return "service 2";
	}
}
