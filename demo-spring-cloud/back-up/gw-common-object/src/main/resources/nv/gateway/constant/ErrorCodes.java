/**
 * 
 */
package gateway.constant;

import java.io.Serializable;

/**
 * @author NV-Dev
 *
 */
public enum ErrorCodes implements Serializable {
	SUCCESS,
    FAIL,
    PENDING,
    NOT_FOUND,
    API_NOT_FOUND,
    EXCEPTION,
    SERVICE_ENDPOINT_INVALID,
    IP_ADDRESS_INVALID,
    CHECKSUM_INVALID,
    HTTP_AUTHEN_INVALID,
    SC_UNAUTHORIZED,
    SC_NOT_ACCEPTABLE,
    ACCESS_DENIED,
    PARAM_DATA_INVALID,
    REQUEST_BODY_INVALID,
    INTERNAL_ERROR,
    ENCRYPT_ERROR,
    DECRYPT_ERROR,
    //AdminAdminRight
    ADMIN_ADMINRIGHT_EXISTED,
    ADMIN_ADMINRIGHT_NOT_EXIST,
    ADMIN_ADMINRIGHT_PASSWORD_INCORRECT,
    ADMIN_ADMINRIGHT_INACTIVE,
    ADMIN_ADMINRIGHT_LOCKED,
    ADMIN_ADMINRIGHT_DELETED,
    //MERCHANT
    MERCHANT_EXISTED,
    MERCHANT_NOT_EXIST,
    MERCHANT_INACTIVE,
    MERCHANT_LOCKED,
    MERCHANT_PASSWORD_INCORRECT,
    MERCHANT_INVALID,
    //PAYMENT_CHANNEL
    PAYMENT_CHANNEL_EXISTED,
    PAYMENT_CHANNEL_NOT_EXIST,
    PAYMENT_CHANNEL_INACTIVE,
    PAYMENT_CHANNEL_DELETED,
    //PAYMENT_CHANNEL_DIRECT
    PAYMENT_CHANNEL_DIRECT_EXISTED,
    PAYMENT_CHANNEL_DIRECT_NOT_EXIST,
    PAYMENT_CHANNEL_DIRECT_INACTIVE,
    //PAYMENT_CHANNEL_FUNCTION
    PAYMENT_CHANNEL_FUNCTION_EXISTED,
    PAYMENT_CHANNEL_FUNCTION_NOT_EXIST,
    PAYMENT_CHANNEL_FUNCTION_INACTIVE,
    //ADMIN
    ADMIN_EXISTED,
    ADMIN_NOT_EXIST,
    ADMIN_INACTIVE,
    ADMIN_DELETED,
    ADMIN_LOGIN_FAILED,
    //ADMIN_RIGHT
    ADMIN_RIGHT_EXISTED,
    ADMIN_RIGHT_NOT_EXIST,
    ADMIN_RIGHT_INACTIVE,
    ADMIN_RIGHT_DELETED,
    //CONFIGURATION
    CONFIGURATION_INACTIVE,
    CONFIGURATION_EXISTED,
    CONFIGURATION_NOT_EXIST,
    //TRANSACTION
    TRANSACTION_EXISTED,
    TRANSACTION_NOT_EXIST,
    TRANSACTION_INVALID,
    TRANSACTION_STATUS_INVALID,
    CANNOT_TRANFER_YOUSELF,
    QUERY_NOT_PROCESS,
    //ISSUER
    ISSUER_EXISTED,
    ISSUER_NOT_EXIST,
    ISSUER_INACTIVE,
    //Merchant error code
    ERROR_CODE_EXISTED,
    ERROR_CODE_NOT_EXIST,
    RECONCILE_EXISTED,
    ORDERS_EXISTED,
    CUSTOMER_LOGIN_FAILED,
    MESSAGE_TEMPLATE_EXISTED,
    PARTNER_EXISTED,
    SERVICE_ENDPOINT_EXISTED,
    CASHOUT_REQUEST_APPROVED,
    FEE_NOT_EXIST,
    DEBIT_RECEIPT_EXITSTED,
    GL_ACCOUNT_EXISTED,
    GL_ACCOUNT_NOT_EXIST,
    GL_ACCOUNT_BALANCE_NOT_ENOUGH,
    EXPENSES_NOT_EXIST,
    SERVICE_ENDPOINT_NOT_EXIST,
    RECONCILE_INVALID,
    GL_ACCOUNT_TYPE_EXISTED,
    MERCHANT_LOCKED_MAX_FAILED,
}
