/**
 * 
 */
package gateway.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author YenDV
 *
 */
@Data
@EqualsAndHashCode
public class DestroyProfileRequestObject {
	private String token;
	private String tokenIssueDate;
	private String requestId;
	private String providerId;
	private String merchantId;
	private String channel;
}
