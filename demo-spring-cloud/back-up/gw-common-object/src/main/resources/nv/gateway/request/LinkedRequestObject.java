	/**
 * 
 */
package gateway.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author YenDV
 * 
 */
@Data
@EqualsAndHashCode
public class LinkedRequestObject {
	private String transactionId; //mã giao dịch của NVPay.
	private String cardNumber;
	private String cardHolderName;
	private String cardIssue;
	private String expiredDateCard;
	private String customerId; // Id của khách hàng trên hệ thống của NVPay.
	private String channel;
}
