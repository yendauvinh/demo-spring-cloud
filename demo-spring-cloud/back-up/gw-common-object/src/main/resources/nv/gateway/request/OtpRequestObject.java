/**
 * 
 */
package gateway.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author YenDV
 *
 *nếu giao dịch cần xác thực otp của NH thì mới sử dụng hàm này.
 */
@Data
@EqualsAndHashCode
public class OtpRequestObject {
	private String transactionId;//transaction của NVPay.
	private String refTransactionId;//transaction của giao dịch cần xác thực.
	private String otp;
	private String channel;
}
