/**
 * 
 */
package gateway.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author YenDV
 *
 */
@Data
@EqualsAndHashCode
public class QueryRequestObject {
	private String transactionId;//mã giao dịch của NVPay.
	private String customerId; // Id của khách hàng trên hệ thống của NVPay.
	private String queryType;
	private String channel;
}
