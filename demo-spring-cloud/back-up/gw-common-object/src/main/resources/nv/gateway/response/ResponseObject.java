package gateway.response;

import java.util.Collection;

import gateway.constant.ErrorCodes;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@EqualsAndHashCode
@ToString
public class ResponseObject {
	@Getter @Setter
	private String code;
	@Getter @Setter
	private String message;
	@Getter @Setter
	private Object data;
	@Getter @Setter
	private Boolean isSuccess;
	
	public ResponseObject() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ResponseObject(Collection<?> results){
		processResponseObject(results);
	}
	
	public ResponseObject(String code, String message, Object data, Boolean isSuccess) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
		this.isSuccess = isSuccess;
	}
	
	private void processResponseObject(Object result){
		if(result == null){
			this.code = ErrorCodes.FAIL.toString();
			this.isSuccess = false;
		}else{
			this.code = ErrorCodes.SUCCESS.toString();
			this.isSuccess = true;
			this.data = result;
		}
	}
}
