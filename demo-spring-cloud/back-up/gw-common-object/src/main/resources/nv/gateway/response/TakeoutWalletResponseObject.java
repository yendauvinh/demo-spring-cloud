/**
 * 
 */
package gateway.response;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author NV-Dev
 *
 */
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class TakeoutWalletResponseObject {
	@Getter @Setter
	private String providerId;
	@Getter @Setter
	private String merchantId;
}
