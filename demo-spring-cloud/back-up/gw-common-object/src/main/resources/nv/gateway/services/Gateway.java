/**
 * 
 */
package gateway.services;

import gateway.request.DestroyProfileRequestObject;
import gateway.request.LinkedRequestObject;
import gateway.request.OtpRequestObject;
import gateway.request.PaymentRequestObject;
import gateway.request.QueryRequestObject;
import gateway.request.TakeoutWalletRequestObject;
import gateway.response.ResponseObject;

/**
 * @author YenDV
 *
 */
public abstract class Gateway {
	/**
	 * <b><i>Method uses for to destroy profile in bank.</i></b>
	 * @param DestroyProfileRequestObject
	 * @return ResponseObject
	 * */
	public ResponseObject destroyProfile(DestroyProfileRequestObject request) {return null;}
	
	/**
	 * <b><i>Method uses for to linked bank card with wallet account.</i></b>
	 * @param LinkedRequestObject
	 * @return ResponseObject
	 * */
	public ResponseObject linkCard(LinkedRequestObject request) {return null;}
	
	/**
	 * <b><i>Method uses for to get bank's otp code.</i></b>
	 * @param OtpRequestObject
	 * @return ResponseObject
	 * */
	public ResponseObject getOtp(OtpRequestObject request) {return null;}
	
	/**
	 * <b><i>Method uses for to payment(cash-in).</i></b>
	 * @param PaymentRequestObject
	 * @return ResponseObject
	 * */
	public ResponseObject cashIn(PaymentRequestObject request) {return null;}
	
	/**
	 * <b><i>Method uses for to take out money from wallet to bank.</i></b>
	 * @param TakeoutWalletRequestObject
	 * @return ResponseObject
	 * */
	public ResponseObject cashOut(TakeoutWalletRequestObject request) {return null;}
	
	/**
	 * <b><i>Method uses for checking link account wallet with card's bank.</i></b>
	 * @param LinkedRequestObject
	 * @return ResponseObject
	 * */
	public ResponseObject checkActiveStatus(LinkedRequestObject req) {return null;}
	
	/**
	 * <b><i>Method uses for checking status of transaction status.</i></b>
	 * @param QueryRequestObject
	 * @return ResponseObject
	 * */
	public ResponseObject query(QueryRequestObject req) {return null;}
}
