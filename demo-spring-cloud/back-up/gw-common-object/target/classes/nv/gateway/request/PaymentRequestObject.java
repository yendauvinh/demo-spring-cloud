/**
 * 
 */
package gateway.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author YenDV
 *
 */
@Data
@EqualsAndHashCode
public class PaymentRequestObject {
	private String requestId;
	private String token;
	private String tokenIssueDate;
	private String amount;
	private String channel;
	private String currencyCode;
	private String desription;
}
