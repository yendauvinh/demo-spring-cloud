/**
 * 
 */
package gateway.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author YenDV
 *
 */
@Data
@EqualsAndHashCode 
//@EqualsAndHashCode(exclude={"id", "shape"})	
public class TakeoutWalletRequestObject {
	private String requestId; // id trong NV's system.
	private String token;
	private String tokenIssueDate;
	private String channel;
	private String merchantId;
	private String currencyCode;
	private String amount;
	private String description;
	private String typeDestCard;
}
