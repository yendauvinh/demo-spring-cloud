package gateway.response;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class DestroyProfileResponseObject {
	@Getter @Setter
	private String providerId;
	@Getter @Setter
	private String merchantId;
	@Getter @Setter
	private String requestId;
}
