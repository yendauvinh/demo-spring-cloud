/**
 * 
 */
package gateway.response;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author NV-Dev
 *
 */
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class LinkedResponseObject {
	@Getter @Setter
	private String transactionId; //transactionId của NVpay.
	@Getter @Setter
	private String merchantId;// id của NV trên bank's system.
	@Getter @Setter
	private String cardNumber;
	@Getter @Setter
	private String cardHolder;
	@Getter @Setter
	private String expiredDateCard;
	@Getter @Setter
	private String providerId;
	@Getter @Setter
	private String customerId;
}
