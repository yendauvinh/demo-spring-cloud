package gateway.response;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class OtpResponseObject {
	@Getter @Setter
	private String data;
	@Getter @Setter
	private String token; //khi đăng ký liên kết thì verify sẽ nhận được token.
}
