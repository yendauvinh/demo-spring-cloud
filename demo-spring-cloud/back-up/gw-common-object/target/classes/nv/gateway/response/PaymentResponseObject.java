/**
 * 
 */
package gateway.response;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author NV-Dev
 *
 */
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class PaymentResponseObject {
	@Getter @Setter
	private String providerId;
	@Getter @Setter
	private String merchantId;
	@Getter @Setter
	private String requestId;
	@Getter @Setter
	private String bankTransactionId;
}
