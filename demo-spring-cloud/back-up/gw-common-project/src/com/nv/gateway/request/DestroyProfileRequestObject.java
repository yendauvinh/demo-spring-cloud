/**
 * 
 */
package com.nv.gateway.request;

/**
 * @author NV-Dev
 *
 */
public class DestroyProfileRequestObject {
	private String token;
	private String tokenIssueDate;
	private String requestId;
	private String providerId;
	private String merchantId;
	private String channel;
}
