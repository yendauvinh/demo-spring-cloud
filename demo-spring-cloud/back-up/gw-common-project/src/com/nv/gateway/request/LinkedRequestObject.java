/**
 * 
 */
package com.nv.gateway.request;

/**
 * @author NV-Dev
 * 
 * Đây là object request của liên kết thẻ.
 */
public class LinkedRequestObject {
	private String transactionId; //mã giao dịch của NVPay.
	private String cardNumber;
	private String cardHolderName;
	private String cardIssue;
	private String expiredDateCard;
	private String customerId; // Id của khách hàng trên hệ thống của NVPay.
}
