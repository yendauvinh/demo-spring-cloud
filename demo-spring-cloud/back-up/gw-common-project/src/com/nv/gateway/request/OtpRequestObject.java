/**
 * 
 */
package com.nv.gateway.request;

/**
 * @author NV-Dev
 *
 *nếu giao dịch cần xác thực otp của NH thì mới sử dụng hàm này.
 */
public class OtpRequestObject {
	private String transactionId;//transaction của NVPay.
	private String refTransactionId;//transaction của giao dịch cần xác thực.
	private String otp;
}
