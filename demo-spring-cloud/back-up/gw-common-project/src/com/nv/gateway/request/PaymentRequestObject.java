/**
 * 
 */
package com.nv.gateway.request;

/**
 * @author NV-Dev
 *
 */
public class PaymentRequestObject {
	private String token;
	private String tokenIssueDate;
	private String amount;
	private String providerId; // id nhà cung cấp dịch vụ. ví dụ dịch vụ du lịch.
	private String merchantId; // id của NVPAy.
	private String desription;
}
