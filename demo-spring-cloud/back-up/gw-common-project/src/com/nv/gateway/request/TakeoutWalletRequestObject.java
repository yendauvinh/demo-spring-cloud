/**
 * 
 */
package com.nv.gateway.request;

/**
 * @author NV-Dev
 *
 */
public class TakeoutWalletRequestObject {
	private String providerId; // nhà cung cấp dịch vụ = partnerId
	private String userId; // id trong NV's system.
	private String token;
	private String channel;
	private String merchantId;
	private String amount;
}
