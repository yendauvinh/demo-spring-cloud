/**
 * 
 */
package com.nv.gateway.response;

/**
 * @author NV-Dev
 *
 */
public class LinkedResponseObject {
	private String transactionId; //transactionId của NVpay.
	private String merchantId;// id của NV trên bank's system.
	private String cardNumber;
	private String cardHolder;
	private String expiredDateCard;
	private String providerId;
	private String customerId;
}
