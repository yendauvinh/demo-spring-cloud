/**
 * 
 */
package com.nv.gateway.response;

/**
 * @author NV-Dev
 *
 */
public class PaymentResponseObject {
	private String providerId;
	private String merchantId;
	private String requestId;
	private String bankTransactionId;
}
