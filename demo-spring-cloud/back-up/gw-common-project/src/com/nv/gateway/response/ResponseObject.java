package com.nv.gateway.response;

import java.util.Collection;

import com.nv.gateway.constant.ErrorCodes;

public class ResponseObject {
	private String code;
	private String message;
	private Object data;
	private Boolean isSuccess;
	
	public ResponseObject(Collection<?> results){
		processResponseObject(results);
	}
	
	public ResponseObject(String code, String message, Object data, Boolean isSuccess) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
		this.isSuccess = isSuccess;
	}
	
	private void processResponseObject(Object result){
		if(result == null){
			this.code = ErrorCodes.FAIL.toString();
			this.isSuccess = false;
		}else{
			this.code = ErrorCodes.SUCCESS.toString();
			this.isSuccess = true;
			this.data = result;
		}
	}
}
