/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.vcb.constant;


import com.google.common.collect.ImmutableMap;

/**
 *
 * @author nnes
 */
public class Constant {

    public static final String CHECK_ACTIVE = "check_active";
    public static final String ACTIVE = "active";
    public static final String DEACTIVE = "deactive";
    public static final String GET_INFO = "get_info";
    public static final String TOPUP = "topup";

    public static final String SIGN_INVALID = "-1";
    public static final String MAINTAIN_SYSTEM = "-2";
    public static final String SERVICE_NOT_PERMIT = "-3";
    public static final String ERROR_SYSTEM = "-4";
    public static final String PARAM_INVALID = "-5";
    public static final String EXCEPTION = "99";

    public static final ImmutableMap<String,String> FUNC_MAP = ImmutableMap.<String, String>builder()
            .put("check_active", "CheckActive")
            .put("active", "Active")
            .put("deactive", "Deactive")
            .put("get_info", "GetInfo")
            .put("topup", "Topup")
            .build();

}
