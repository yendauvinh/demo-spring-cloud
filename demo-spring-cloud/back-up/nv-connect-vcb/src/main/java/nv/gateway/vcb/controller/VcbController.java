/**
 * 
 */
package nv.gateway.vcb.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gateway.request.LinkedRequestObject;
import gateway.request.PaymentRequestObject;
import gateway.request.QueryRequestObject;
import gateway.request.TakeoutWalletRequestObject;
import gateway.response.ResponseObject;

/**
 * @author NV-Dev
 *
 */
@RestController
public class VcbController {
	//@Autowired
	//private VCBServicesImpl vcbService;
	
	@RequestMapping(value = "/cashIn" , method = RequestMethod.POST)
	public ResponseObject cashIn(@RequestBody PaymentRequestObject request){
		//return vcbService.cashIn(request);
		return hashResult();
	}
	
	@RequestMapping(value = "/cashOut" , method = RequestMethod.POST)
	public ResponseObject cashOut(@RequestBody TakeoutWalletRequestObject request){
		//return vcbService.cashOut(request);
		return hashResult();
	}
	
	@RequestMapping(value = "/checkActiveStatus" , method = RequestMethod.POST)
	public ResponseObject checkActiveStatus(@RequestBody LinkedRequestObject request){
		//return vcbService.checkActiveStatus(request);
		return hashResult();
	}
	
	@RequestMapping(value = "/query" , method = RequestMethod.POST)
	public ResponseObject query(@RequestBody QueryRequestObject request){
		//return vcbService.query(request);
		return hashResult();
	}
	
	private ResponseObject hashResult(){
		ResponseObject resObj = new ResponseObject();
		resObj.setCode("SUCCESSS");
		resObj.setData("");
		resObj.setIsSuccess(true);
		resObj.setMessage("success");
		return resObj;
	}
}
