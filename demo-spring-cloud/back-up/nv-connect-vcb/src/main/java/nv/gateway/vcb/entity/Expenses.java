/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.vcb.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author nnes
 */
@Entity
@Table(name = "EXPENSES")
@NamedQueries({
    @NamedQuery(name = "Expenses.findAll", query = "SELECT e FROM Expenses e")})
public class Expenses implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_Sequence")
    @SequenceGenerator(name = "id_Sequence", sequenceName = "SEQ_EXPENSES", allocationSize = 1)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PARTNER_ID")
    private Long partnerId;
    @Basic(optional = false)
    @Column(name = "PARTNER_CODE")
    private String partnerCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRICE_PERCENTAGE")
    private Double pricePercentage;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRICE_FIXED")
    private Double priceFixed;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIME_CREATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeCreated;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIME_BEGIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeBegin;
    @Column(name = "TIME_END")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeEnd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private Short status;
    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "ISSUER_ID")
    private Long issuerId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "LIMIT_MIN")
    private Double limitMin;
    @Column(name = "LIMIT_MAX")
    private Double limitMax;

    public Expenses() {
    }

    public Expenses(Long id) {
        this.id = id;
    }

    public Expenses(Double priceFixed, Double pricePercentage) {
        this.pricePercentage = pricePercentage;
        this.priceFixed = priceFixed;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public double getPricePercentage() {
        return pricePercentage;
    }

    public void setPricePercentage(Double pricePercentage) {
        this.pricePercentage = pricePercentage;
    }

    public double getPriceFixed() {
        return priceFixed;
    }

    public void setPriceFixed(Double priceFixed) {
        this.priceFixed = priceFixed;
    }

    public Date getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Date timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Date getTimeBegin() {
        return timeBegin;
    }

    public void setTimeBegin(Date timeBegin) {
        this.timeBegin = timeBegin;
    }

    public Date getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getIssuerId() {
        return issuerId;
    }

    public void setIssuerId(Long issuerId) {
        this.issuerId = issuerId;
    }

    public Double getLimitMin() {
        return limitMin;
    }

    public void setLimitMin(Double limitMin) {
        this.limitMin = limitMin;
    }

    public Double getLimitMax() {
        return limitMax;
    }

    public void setLimitMax(Double limitMax) {
        this.limitMax = limitMax;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Expenses)) {
            return false;
        }
        Expenses other = (Expenses) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "com.nvpay.entity.Expenses[ id=" + id + " ]";
    }

}
