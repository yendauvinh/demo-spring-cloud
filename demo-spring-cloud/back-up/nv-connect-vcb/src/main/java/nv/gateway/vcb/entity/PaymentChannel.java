/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.vcb.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lenovo
 */
@Entity
@Table(name = "PAYMENT_CHANNEL")
@NamedQueries({
    @NamedQuery(name = "PaymentChannel.findAll", query = "SELECT p FROM PaymentChannel p")})
public class PaymentChannel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_Sequence")
    @SequenceGenerator(name = "id_Sequence", sequenceName = "SEQ_PAYMENT_CHANNEL", allocationSize = 1)
    @Basic(optional = false)
    @NotNull
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "PAYMENT_CHANNEL_CODE")
    private String paymentChannelCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    private String url;
    private Long port;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "HANDLER_CLASS")
    private String handlerClass;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "HANDLER_FILE_URL")
    private String handlerFileUrl;
    @Size(max = 255)
    private String username;
    @Size(max = 255)
    private String password;
    @Size(max = 255)
    @Column(name = "KEY_AUTHEN")
    private String keyAuthen;
    @Size(max = 255)
    @Column(name = "KEY_ENCRYPT")
    private String keyEncrypt;
    @Size(max = 255)
    @Column(name = "KEY_PUBLIC")
    private String keyPublic;
    @Size(max = 255)
    @Column(name = "KEY_PRIVATE")
    private String keyPrivate;
    @Size(max = 255)
    @Column(name = "DATA_INFO")
    private String dataInfo;
    @Basic(optional = false)
    @NotNull
    private Short status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIME_CREATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeCreated;
    @Column(name = "TIME_UPDATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeUpdated;
    @Size(max = 255)
    private String description;
    @Column(name = "PARTNER_ID")
    private Long partnerId;
    @JsonIgnore
    @JoinColumn(name = "PARTNER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Partner partner;

    public PaymentChannel() {
    }

    public PaymentChannel(Long id) {
        this.id = id;
    }

    public PaymentChannel(Long id, String paymentChannelCode, String url) {
        this.id = id;
        this.paymentChannelCode = paymentChannelCode;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPaymentChannelCode() {
        return paymentChannelCode;
    }

    public void setPaymentChannelCode(String paymentChannelCode) {
        this.paymentChannelCode = paymentChannelCode;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getPort() {
        return port;
    }

    public void setPort(Long port) {
        this.port = port;
    }

    public String getHandlerClass() {
        return handlerClass;
    }

    public void setHandlerClass(String handlerClass) {
        this.handlerClass = handlerClass;
    }

    public String getHandlerFileUrl() {
        return handlerFileUrl;
    }

    public void setHandlerFileUrl(String handlerFileUrl) {
        this.handlerFileUrl = handlerFileUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getKeyAuthen() {
        return keyAuthen;
    }

    public void setKeyAuthen(String keyAuthen) {
        this.keyAuthen = keyAuthen;
    }

    public String getKeyEncrypt() {
        return keyEncrypt;
    }

    public void setKeyEncrypt(String keyEncrypt) {
        this.keyEncrypt = keyEncrypt;
    }

    public String getKeyPublic() {
        return keyPublic;
    }

    public void setKeyPublic(String keyPublic) {
        this.keyPublic = keyPublic;
    }

    public String getKeyPrivate() {
        return keyPrivate;
    }

    public void setKeyPrivate(String keyPrivate) {
        this.keyPrivate = keyPrivate;
    }

    public String getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(String dataInfo) {
        this.dataInfo = dataInfo;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public Date getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Date timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Date getTimeUpdated() {
        return timeUpdated;
    }

    public void setTimeUpdated(Date timeUpdated) {
        this.timeUpdated = timeUpdated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentChannel)) {
            return false;
        }
        PaymentChannel other = (PaymentChannel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.nvpay.entity.PaymentChannel[ id=" + id + " ]";
    }

}
