/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.vcb.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Lenovo
 */
@Entity
@Table(name = "PAYMENT_CHANNEL_DIRECT")
@NamedQueries({
    @NamedQuery(name = "PaymentChannelDirect.findAll", query = "SELECT p FROM PaymentChannelDirect p")})
public class PaymentChannelDirect implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_Sequence")
    @SequenceGenerator(name = "id_Sequence", sequenceName = "SEQ_PAYMENT_CHANNEL_DIRECT", allocationSize = 1)
    @Basic(optional = false)
    @NotNull
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISSUER_ID")
    private Long issuerId;
    @Basic(optional = false)
    @NotNull
    private Short status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIME_CREATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeCreated;
    @Column(name = "TIME_UPDATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeUpdated;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PAYMENT_CHANNEL_ID")
    private Long paymentChannelId;
    @JoinColumn(name = "PAYMENT_CHANNEL_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private PaymentChannel paymentChannel;

    public PaymentChannelDirect() {
    }

    public PaymentChannelDirect(Long id) {
        this.id = id;
    }

    public PaymentChannelDirect(Long id, long issuerId, short status, Date timeCreated) {
        this.id = id;
        this.issuerId = issuerId;
        this.status = status;
        this.timeCreated = timeCreated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getIssuerId() {
        return issuerId;
    }

    public void setIssuerId(long issuerId) {
        this.issuerId = issuerId;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public Date getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Date timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Date getTimeUpdated() {
        return timeUpdated;
    }

    public void setTimeUpdated(Date timeUpdated) {
        this.timeUpdated = timeUpdated;
    }

    public long getPaymentChannelId() {
        return paymentChannelId;
    }

    public void setPaymentChannelId(long paymentChannelId) {
        this.paymentChannelId = paymentChannelId;
    }

    public PaymentChannel getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(PaymentChannel paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentChannelDirect)) {
            return false;
        }
        PaymentChannelDirect other = (PaymentChannelDirect) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.nvpay.entity.PaymentChannelDirect[ id=" + id + " ]";
    }

}
