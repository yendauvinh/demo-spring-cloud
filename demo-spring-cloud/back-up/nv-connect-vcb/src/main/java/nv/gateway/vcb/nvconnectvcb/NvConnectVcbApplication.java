package nv.gateway.vcb.nvconnectvcb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan("nv.gateway.vcb")
public class NvConnectVcbApplication {
	public static void main(String[] args) {
		SpringApplication.run(NvConnectVcbApplication.class, args);
	}
}
