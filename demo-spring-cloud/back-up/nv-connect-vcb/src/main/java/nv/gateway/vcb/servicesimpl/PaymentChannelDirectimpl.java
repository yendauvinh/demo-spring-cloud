/**
 * 
 */
package nv.gateway.vcb.servicesimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import nv.gateway.vcb.entity.PaymentChannelDirect;
import nv.gateway.vcb.repository.PaymentChannelDirectRepository;
import nv.gateway.vcb.services.PaymentChannelDirectServices;

/**
 * @author NV-Dev
 *
 */
@Service
public class PaymentChannelDirectimpl implements PaymentChannelDirectServices {
	//@Autowired
	private PaymentChannelDirectRepository pmCnDrRepo; 
	
	//@Value("${channel.code}")
    private String channelCode;
	
	@Override
	//@CacheEvict(key="#payment_config" , value = "pmCnInfo")
	public PaymentChannelDirect getInfomationPmCnDr() {
		PaymentChannelDirect pmCnInfo = null;
		try {
			pmCnInfo = pmCnDrRepo.getPmCnDrByPaymentChannelCode(channelCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pmCnInfo;
	}

}
