package nv.gateway.vcb.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.codec.binary.Base64;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpUtil {

    private static final Logger logger = LogManager.getLogger(HttpUtil.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    public static String send(String url, Map<String, Object> map, String userPass) throws IOException {
        String query = getParameterString(map);
        return send(url, query, userPass);
    }

    public static String send(String url, Map<String, Object> paramMap) throws IOException {
        return send(url, paramMap, null);
    }
    
    public static String sendRequest(String url, String param, String userPass) throws IOException {
        return send(url, param, userPass);
    }

//    public static <T> String send(String url, T obj, String userPass) throws IOException {
//        logger.info(11111);
//        try {
//            String query = mapper.convertValue(obj, QueryObj.class).toString();
//            return send(url, query, userPass);
//        } catch (IllegalArgumentException | IOException ex) {
//            logger.info(ex.getMessage());
//            throw ex;
//        }
//    }

    private static String send(String Url, String query, String userPass) throws IOException {
        try {
            URL url = new URL(Url);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            if (userPass != null) {
                String basicAuth = "Basic " + new String(new Base64().encode(userPass.getBytes()));
                con.setRequestProperty("Authorization", basicAuth);
            }
            con.setRequestMethod("POST");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Accept-Charset", "UTF-8");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setConnectTimeout(30000);
            con.setReadTimeout(30000);

            try (DataOutputStream wr = new DataOutputStream((con.getOutputStream()))) {
                wr.writeBytes(query);
                wr.flush();
                wr.close();
            }
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                StringBuilder response = new StringBuilder();
                try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"))) {
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                }
                return response.toString();
            }

            return null;
        } catch (SocketTimeoutException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        }
    }

    public static String getParameterString(Map<String, Object> map) {
        StringBuilder paramString = new StringBuilder();
        for (Entry<String, Object> entry : map.entrySet()) {
            paramString.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        System.out.println(paramString.toString());
        return paramString.toString();
    }
}

class QueryObj {

    private StringBuilder builder = new StringBuilder();

    @JsonAnySetter
    public void addToUri(String name, Object property) {
        if (builder.length() > 0) {
            builder.append("&");
        }
        if (property != null) {
            builder.append(name).append("=").append(property);
        }

    }

    @Override
    public String toString() {
        return builder.toString();
    }
}
