package nv.gateway.vtb.entity;

@SuppressWarnings("serial")
public class RevokeTokenReq extends AbstractRequest {

	private String token;
	private String tokenIssueDate;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTokenIssueDate() {
		return tokenIssueDate;
	}

	public void setTokenIssueDate(String tokenIssueDate) {
		this.tokenIssueDate = tokenIssueDate;
	}

}
