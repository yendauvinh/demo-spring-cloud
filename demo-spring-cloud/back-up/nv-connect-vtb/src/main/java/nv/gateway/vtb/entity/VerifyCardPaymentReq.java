package nv.gateway.vtb.entity;

@SuppressWarnings("serial")
public class VerifyCardPaymentReq extends AbstractRequest {

	private String cardNumber;
	private String cardIssueDate;
	private String cardHolderName;
	private String amount;
	private String currencyCode;

	private String providerCustId;
	private String custPhoneNo;
	private String custIDNo;

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardIssueDate() {
		return cardIssueDate;
	}

	public void setCardIssueDate(String cardIssueDate) {
		this.cardIssueDate = cardIssueDate;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getProviderCustId() {
		return providerCustId;
	}

	public void setProviderCustId(String providerCustId) {
		this.providerCustId = providerCustId;
	}

	public String getCustPhoneNo() {
		return custPhoneNo;
	}

	public void setCustPhoneNo(String custPhoneNo) {
		this.custPhoneNo = custPhoneNo;
	}

	public String getCustIDNo() {
		return custIDNo;
	}

	public void setCustIDNo(String custIDNo) {
		this.custIDNo = custIDNo;
	}

}
