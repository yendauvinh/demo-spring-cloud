package nv.gateway.vtb.nvconnectvtb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan("nv.gateway.vtb")
public class NvConnectVtbApplication {

	public static void main(String[] args) {
		SpringApplication.run(NvConnectVtbApplication.class, args);
	}
}
