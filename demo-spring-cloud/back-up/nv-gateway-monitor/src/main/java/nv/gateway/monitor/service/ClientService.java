/**
 * 
 */
package nv.gateway.monitor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

/**
 * @author NV-Dev
 *
 */
@Service
public class ClientService {
	@Autowired
	private EurekaClient eurekaClient;
	
	@HystrixCommand(fallbackMethod = "failed" )
	public String checkConnection(String urlDetails){
		String result = ""; 
		InstanceInfo instanceInfo = eurekaClient.getNextServerFromEureka("api-2", false);
		String serviceBaseUrl = instanceInfo.getHomePageUrl();
		result = new RestTemplate().getForObject(serviceBaseUrl + urlDetails, String.class);
		return result;
	}
	
	public String failed(String urlDetails){
		return "failed";
	}
}
