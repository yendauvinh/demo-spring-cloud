/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.exception;

/**
 *
 * @author yendv
 */
public class BaseException extends RuntimeException {

    private Object[] arr;

    public BaseException(String message) {
        super(message, null, false, false);
    }

    public BaseException(String message, Object... arr) {
        super(message, null, false, false);
        this.arr = arr;
    }

    public BaseException(String message, Throwable t) {
        super(message, t, false, false);
    }

    public BaseException(String message, Throwable t, Object... arr) {
        super(message, t, false, false);
        this.arr = arr;
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

    public Object[] getArr() {
        return arr;
    }

    public void setArr(Object[] arr) {
        this.arr = arr;
    }

}
