/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.exception;

/**
 *
 * @author yendv
 */
public class PGException extends BaseException {

    public PGException(String message) {
        super(message);
    }

    public PGException(String message, Object... arr) {
        super(message, arr);
    }

    public PGException(String message, Throwable t) {
        super(message, t);
    }

    public PGException(String message, Throwable t, Object... arr) {
        super(message, t, arr);
    }
}
