/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.object;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

/**
 *
 * @author bachnt
 */
@Setter
@Getter
public class HttpObject {

    private String uri;
    private Object object;
    private String httpUser;
    private String httpPassword;
    private String contentType = MediaType.APPLICATION_JSON_UTF8_VALUE;
    private HttpMethod httpMethod;
    private HttpHeaders httpHeader;

    public HttpObject(String uri) {
        this.uri = uri;
    }

    public HttpObject(String uri, Object object) {
        this.uri = uri;
        this.object = object;
    }

    public HttpObject(String uri, Object object, String httpUser, String httpPassword) {
        this.uri = uri;
        this.object = object;
        this.httpUser = httpUser;
        this.httpPassword = httpPassword;
    }

    public HttpObject(String uri, Object object, String httpUser, String httpPassword, String contentType) {
        this.uri = uri;
        this.object = object;
        this.httpUser = httpUser;
        this.httpPassword = httpPassword;
        this.contentType = contentType;
    }

    public HttpObject(String uri, Object object, String contentType) {
        this.uri = uri;
        this.object = object;
        this.contentType = contentType;
    }

    public HttpHeaders getHttpHeader() {
        httpHeader = new HttpHeaders();
        httpHeader.add("Content-Type", contentType);
        httpHeader.add("Accept", "*/*");
        if (httpUser != null && httpPassword != null) {
            String userPassword = httpUser + ":" + httpPassword;
            String basicAuth = "Basic " + new String(new Base64().encode(userPassword.getBytes()));
            httpHeader.add("Authorization", basicAuth);
        }
        return httpHeader;
    }

    public boolean isGet() {
        return HttpMethod.GET == this.httpMethod;
    }

    public boolean isPost() {
        return HttpMethod.POST == this.httpMethod;
    }

    public boolean isPut() {
        return HttpMethod.PUT == this.httpMethod;
    }
}
