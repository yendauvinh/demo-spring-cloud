package nv.gateway.object;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class PGResponseObject {

    private boolean success = false;
    private boolean timeout = false;
    private boolean exeption = false;
    private String description;
    private Object result;

    @JsonIgnore
    public <T> PGResponseObject(T result) {
        processResponseObject(result);
    }

     @JsonIgnore
    private void processResponseObject(Object result) {
        if (result != null) {
            this.result = result;
            this.success = true;
        } else {
            this.success = false;
        }
    }
}
