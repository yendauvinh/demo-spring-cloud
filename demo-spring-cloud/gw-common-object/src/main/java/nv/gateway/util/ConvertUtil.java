/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Lenovo
 */
public class ConvertUtil {

    public static final ObjectMapper MAPPER = new ObjectMapper()
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    public static <T> T toObject(String str, Class<T> clazz) {
        try {
            return MAPPER.readValue(str, clazz);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static <T> T toObject(String str, TypeReference<T> dataType) {
        try {
            return MAPPER.readValue(str, dataType);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static <T> T toObject(Object obj, TypeReference<T> dataType) {
        return MAPPER.convertValue(obj, dataType);
    }

    public static String toString(Object obj) {
        if (obj == null) {
            return "";
        }
        try {
            return MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static <T> List<T> toList(String str, Class<T> clazz) {
        try {
            JavaType type = MAPPER.getTypeFactory().constructCollectionType(List.class, clazz);
            return MAPPER.readValue(str, type);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Map toMap(Object... obj) {
        Map<String, Object> map = new HashMap<>();
        for (int i = 0; i < obj.length; i++) {
            if (obj[i + 1] != null) {
                map.put(obj[i].toString(), obj[i + 1]);
            }
            i++;
        }
        return map;
    }
    
    public static String convertObjToStr(Object object){
    	try {
			return MAPPER.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
}
