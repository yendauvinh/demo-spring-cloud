package nv.gateway.util;

import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import nv.gateway.object.HttpObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class HttpUtil {

    public static String get(HttpObject httpObj) {
        return send(httpObj, HttpMethod.GET);
    }

    public static String post(HttpObject httpObj) {
        return send(httpObj, HttpMethod.POST);
    }

    public static String put(HttpObject httpObj) {
        return send(httpObj, HttpMethod.PUT);
    }

    public static <T> T get(HttpObject httpObj, Class<T> clazz) {
        return ConvertUtil.toObject(get(httpObj), clazz);
    }

    public static <T> T post(HttpObject httpObj, Class<T> clazz) {
        return ConvertUtil.toObject(post(httpObj), clazz);
    }

    public static <T> T put(HttpObject httpObj, Class<T> clazz) {
        return ConvertUtil.toObject(put(httpObj), clazz);
    }

    public static String send(HttpObject httpObj, HttpMethod method) {
//        try {
    	String request = ConvertUtil.toString(httpObj.getObject());
    	System.out.println("----------------------------------");
    	System.out.println(httpObj.getUri());
    	System.out.println(request);
    	System.out.println("----------------------------------");
            HttpEntity<String> requestEntity = new HttpEntity<>(
            		request, httpObj.getHttpHeader());
            RestTemplate rest = new RestTemplate();
            ResponseEntity<String> responseEntity = rest.exchange(httpObj.getUri(), method, requestEntity, String.class);
            return responseEntity.getBody();
//        } catch (HttpClientErrorException ex) {
//            ex.printStackTrace();
//            HttpStatus status = ex.getStatusCode();
//            return ex.getResponseBodyAsString();
//        } catch (RestClientException ex) {
//            ex.printStackTrace();
//            if (ex.getRootCause() instanceof SocketTimeoutException) {
//                return null;
//            }
//        }
//        return null;
    }
}
