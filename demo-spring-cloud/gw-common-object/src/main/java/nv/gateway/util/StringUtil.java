/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.util;

import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author Lenovo
 */
public class StringUtil extends StringUtils {

    private static final String ALPHANUM = "123456789abcdefghijkmnpqrstuvxy";
    private static final String NUM = "0123456789";
    private static final SecureRandom SEC_RAND = new SecureRandom();

    public static String getString(Object... array) {
        StringBuilder sb = new StringBuilder();
        for (Object obj : array) {
            sb.append(obj == null ? "" : obj.toString());
        }
        return sb.toString();
    }

    public static String getString(String... array) {
        return String.join("", array);
    }

    public static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(ALPHANUM.charAt(SEC_RAND.nextInt(ALPHANUM.length())));
        }
        return sb.toString();
    }

    public static String randomNumberic(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(NUM.charAt(SEC_RAND.nextInt(NUM.length())));
        }
        return sb.toString();
    }

    public static String amountFormat(Double amount) {
        NumberFormat nf = new DecimalFormat("###,###,###");
        return nf.format(amount);
    }

    public static String removeSign(String str) {
        if (str == null) {
            return null;
        }
        try {
            String temp = Normalizer.normalize(str, Normalizer.Form.NFD);
            Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
            return pattern.matcher(temp).replaceAll("").replaceAll("đ", "d");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return str;
    }

    public static String mobileNumberFormat(String mobileNumber) {
        return String.join("", "0", mobileNumber.substring(2));
    }

    public static String getQueryString(String... array) {
        StringBuilder sb = new StringBuilder();
        int len = array.length;
        for (int i = 0; i < len; i++) {
            if (array[i + 1] != null) {
                sb.append(array[i]).append("=").append(array[i + 1]).append("&");
            }
            i++;
        }
        return sb.toString();
    }
}
