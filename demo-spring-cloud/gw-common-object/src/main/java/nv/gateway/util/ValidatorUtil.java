package nv.gateway.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import nv.gateway.exception.InvalidException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidatorUtil {

    @Autowired
    private Validator baseValidator;

    public <T> Map<String, String> validate(T object) {
        Map<String, String> errors = new HashMap<>();
        if (object == null) {
            return errors;
        }
        Set<ConstraintViolation<T>> constraints = baseValidator.validate(object);
        constraints.forEach((constraint) -> {
            errors.put(constraint.getPropertyPath().toString(), constraint.getMessage());
        });
        return errors;
    }

//    public <T> void validateParam(T object, String... fields) throws NVPayException {
//        if (object == null) {
//            return;
//        }
//         if (fields.length == 0) {
//            Set<ConstraintViolation<T>> constraints = baseValidator.validate(object);
//            constraints.forEach((constraint) -> {
//                throw new NVPayException(constraint.getMessage());
//            });
//        } else {
//            for (String field : fields) {
//                Set<ConstraintViolation<T>> constraints = baseValidator.validateProperty(object, field);
//                constraints.forEach((constraint) -> {
//                    throw new NVPayException(constraint.getMessage());
//                });
//            }
//        }
//    }

    public <T> void validateParam(T object, String... fields) throws InvalidException {
        if (object == null) {
            return;
        }
        if (fields.length == 0) {
            Set<ConstraintViolation<T>> constraints = baseValidator.validate(object);
            constraints.forEach((constraint) -> {
                throw new InvalidException(constraint.getPropertyPath().toString());
            });
        } else {
            for (String field : fields) {
                Set<ConstraintViolation<T>> constraints = baseValidator.validateProperty(object, field);
                constraints.forEach((constraint) -> {
                    throw new InvalidException(constraint.getPropertyPath().toString());
                });
            }
        }
    }

}
