/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.vcb.constant;

import java.io.StringReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.Signature;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

import nv.gateway.vcb.utils.PasswordDeriveBytes;

/**
 *
 * @author nnes
 */
public class Security {

    static String iv = "%cr9Z4ial*I$hJkl";
    static String passPhase = "p%Cf5S^z";
    static String salt = "a@VE1W2%";
    static String privateKey = "<RSAKeyValue><Modulus>1oPm62PKa/ZanWo0AIiayPYxPDK7JSB3mWlzaCnXe1ndgYqfAZ1gshfTTbZQroKTks3t9QAtYipmI1AI+UjIYxfqDlH4BznunhQ/KUReBDjVdiPmQHYxTyJTKP0IJE3/SG1Jyb9nVziCCI1S0TKM5rZhYrHp/ShltPux7FxSC4jBL8tIMKl8ct0r0jVM273IKnRRwntHy0eTvBHkzxFAzvbeYK4z4EHzUk6JuW8s/PlT5BTfnibJbpxrxhyJD+LhwC0uoo5QfN3JnLCvdNW0n4/GmCU3C3+GSGLfzlyoI+xOjEwZ5/TA2KsBFzVnQ9SyQ3i9l8k9qKXr+id8eU6PAw==</Modulus><Exponent>AQAB</Exponent><P>7Xp2o3bYgVitNyWLob0363+GNcqMgCZOnHjZVUGw5vUotUwVDJsjrGI/WpONnGROAXYZLIT1OGap2A5jBYfz6X3jqywLL3BQyW3AzvCC6zYFgghJ3FpTWQesYqBUu5UMi8dg++msinZp3b1a1BtXVctwKBVvT93TB1uOU3IfXx8=</P><Q>5z7z2ajAFDKZ5R2YxKNuc4fBRbIDlyXYN5iWwQUuiXloVe6/xG3yhROgIXbV054po+G/LLIBUP1VVHwR/KnBgmgFWmxRGpyFES7k21SKzK0f8K10ecIHChpWwqChiuqmoGgNW04ISi0y4iL+WGE8I0f/5vS1AUIA7fiS4M58p50=</Q><DP>Vr+ZfvT2+aju/WoVq6t3NeuLlzK6SEW2CaqLmX05peSATlnQhp5VVa89/VRRMoPhit8dKwoWmdfGy0nWGjsjenOmKu0X2OhqWmphODbbXBiLwohMktXcBHWtHwn6adt0jcrzPpKJmLrQyVNWToz+RjTeb4YE92PMpq9nOkxB8Xk=</DP><DQ>zLyH2aX2qetDJf9WSoMIP3FqRPKfCyuOi2qlzq9vzHeZULvZPWi61+fQVtCH8JyP8XDlegT/9VzWN77BjhoeIf9G/g/4nr8FcXk7VpU7bou5Q7XO8h4/bUp0lkzxubSXYl29taEY5dOuwViKKGWugO42O51cxIhTml4CbNdktmE=</DQ><InverseQ>mn+5VvmP3jeja8Mks7xeI5fQTl4F/RHWNv5Htkz/PJti4tTlHnP51XEEwQ8TrMNRBy4mhcu5Q1jx+EIZAmhh+HWm1ezrmaYSpKj2xugpN5YCipWbxmu/5P2AKz6FTNAuPHPk8DLwZZj0uK7oZNITr/6goRCYoFIqbQmuXsX+KdE=</InverseQ><D>WqQpctUbtzrFVzaE+qS+IqREcZdOFIX/g0dXiHAFqQ1z10TnoxsIvLfshS3AMbzi9mklXxzQEGax+cHbyNnEMnrsX1irYQJNnWYYuis1Prt1FxJcPGi0tSdf1DX2tsBuWgAz6ZeC5wvthgeRMuwVNXdi9v0OTfY/45VmvOTVliFQ0URdCb0GlFyH/n+5o9Fkj4Rmleym9VC1LDhquImz7seXzKpqZVkL9UTHl2tyUrdxNbhtYjS1GscO7PoAtuimn6+lV7WDmtr/VhlR+7qZRFP5VfT6npImpBRXE2BraaTqrCrYFPktqc14KOItw/v/WhiRCTDi8X0mp0iIQ8zMYQ==</D></RSAKeyValue>";
    static String publicKey = "";

    public static String encrypt(String plaintext) throws Exception {
        final PasswordDeriveBytes pass = new PasswordDeriveBytes(getPassPhase(), getSalt().getBytes("UTF-8"));
        byte[] key = pass.getBytes(256 / Byte.SIZE);
        SecretKeySpec specKey = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, specKey, new IvParameterSpec(getIv().getBytes("UTF-8")));
        byte[] ciphertext = cipher.doFinal(plaintext.getBytes());
        return Base64.encodeBase64String(ciphertext);
    }

    public static String decrypt(String encrypted) throws Exception {
        final PasswordDeriveBytes pass = new PasswordDeriveBytes(getPassPhase(), getSalt().getBytes("UTF-8"));
        byte[] key = pass.getBytes(256 / Byte.SIZE);
        SecretKeySpec specKey = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, specKey, new IvParameterSpec(getIv().getBytes("UTF-8")));
        return new String(cipher.doFinal(Base64.decodeBase64(encrypted.getBytes("UTF-8"))), "UTF-8");
    }

    static {
        // org.apache.xml.security.Init.init();
        try {
            java.lang.reflect.Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
            field.setAccessible(true);
            field.set(null, java.lang.Boolean.FALSE);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String sign(String input) throws Exception {
        Signature signature = initSign();
        signature.update(input.getBytes());
        String result = Base64.encodeBase64String(signature.sign());
        return result;
    }

    public static boolean verify(String data, String sig) throws Exception {
        Signature signature = initVerify();
        signature.update(data.getBytes());
        return signature.verify(Base64.decodeBase64(sig));
    }

    private static RSAPrivateKeySpec getPrivKey() throws Exception {
        SAXBuilder sb = new SAXBuilder();
        Document doc = sb.build(new InputSource(new StringReader(getPrivateKey())));

        Element root = doc.getRootElement();
        Element modulusElem = root.getChild("Modulus");
        Element dElem = root.getChild("D");
        byte[] modBytes = Base64.decodeBase64(modulusElem.getText().trim().getBytes());
        byte[] dBytes = Base64.decodeBase64(dElem.getText().trim().getBytes());

        BigInteger modules = new BigInteger(1, modBytes);
        BigInteger d = new BigInteger(1, dBytes);

        RSAPrivateKeySpec privSpec = new RSAPrivateKeySpec(modules, d);
        return privSpec;
    }

    private static RSAPublicKeySpec getPubKey() throws Exception {
        SAXBuilder sb = new SAXBuilder();
        Document doc = sb.build(new InputSource(new StringReader(getPublicKey())));

        Element root = doc.getRootElement();
        Element modulusElem = root.getChild("Modulus");
        Element dElem = root.getChild("D");
        byte[] modBytes = Base64.decodeBase64(modulusElem.getText().trim().getBytes());
        byte[] dBytes = Base64.decodeBase64(dElem.getText().trim().getBytes());

        BigInteger modules = new BigInteger(1, modBytes);
        BigInteger d = new BigInteger(1, dBytes);

        RSAPublicKeySpec pubSpec = new RSAPublicKeySpec(modules, d);
        return pubSpec;
    }

    private static Signature initSign() throws Exception {
        KeyFactory factory = KeyFactory.getInstance("RSA");
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initSign(factory.generatePrivate(getPrivKey()));
        return signature;
    }

    private static Signature initVerify() throws Exception {
        KeyFactory factory = KeyFactory.getInstance("RSA");
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initVerify(factory.generatePublic(getPubKey()));
        return signature;
    }

    public static String getIv() {
        return iv;
    }

    public static void setIv(String iv) {
        Security.iv = iv;
    }

    public static String getPassPhase() {
        return passPhase;
    }

    public static void setPassPhase(String passPhase) {
        Security.passPhase = passPhase;
    }

    public static String getSalt() {
        return salt;
    }

    public static void setSalt(String salt) {
        Security.salt = salt;
    }

    public static String getPrivateKey() {
        return privateKey;
    }

    public static void setPrivateKey(String privateKey) {
        Security.privateKey = privateKey;
    }

    public static String getPublicKey() {
        return publicKey;
    }

    public static void setPublicKey(String publicKey) {
        Security.publicKey = publicKey;
    }

    public static void main(String[] args) throws Exception {
//        String test = "abc";
        String encrypted = "uIP44UqoBJhc8f4MJIIghmw9KTru9j4woAl9d8Agaxf2OZQhOubnR8YhN9CrYQuR31GJ9iNf07bvjmZUZo5Qs3E68v8OujBU3TY/92faJxwK7zreqg0ULwvYkwb61bLT4bSZuKGDDa85J/+FkI8Fpw==";
        //  System.out.println(encrypt(test));
        String decrypted = decrypt(encrypted);
        System.out.println(decrypted);
    }

}
