/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.vcb.constant;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.LogLevel;
import microsoft.aspnet.signalr.client.Logger;
import microsoft.aspnet.signalr.client.StateChangedCallback;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import nv.gateway.vcb.entity.Request;
import nv.gateway.vcb.entity.Response;
import nv.gateway.vcb.entity.VcbDataRequest;
import nv.gateway.vcb.entity.VcbDataResponse;
import nv.gateway.vcb.entity.VcbTransDataResponse;
import nv.gateway.vcb.entity.NVCoreRequest;
import nv.gateway.vcb.entity.NVCoreResponse;
import nv.gateway.vcb.entity.NVDataRequest;
import nv.gateway.vcb.utils.HttpUtil;
import nv.gateway.vcb.utils.PGSecurity;

import org.dozer.DozerBeanMapper;
/*import pg.channel.vcb.object.Request;
import pg.channel.vcb.object.Response;
import pg.channel.vcb.object.VcbDataRequest;
import pg.channel.vcb.object.VcbDataResponse;
import pg.channel.vcb.object.VcbTransDataResponse;
import pg.channel.vcb.object.VimoCoreRequest;
import pg.channel.vcb.object.VimoCoreResponse;
import pg.channel.vcb.object.VimoDataRequest;
import pg.util.HttpUtil;
import pg.util.PGSecurity;*/

/**
 *
 * @author nnes
 */
public class SignalRClient {

    private final static String CLIENT_VERSION = "1.0";

    private String url = "http://192.168.200.234:8080/";
    private String hubProxy = "IpcHub";
    private String clientId = "VIMO";
    private String passCode = "1234";
    // private String vimoUrl = "http://192.168.11.18:8080/vimo_enhancement/api/vcb";
    private String vimoUrl = "http://sandbox.vimo.vn/vcb.php";
    private String vimoUrlUserPass = "vimoussd:vimoussd260285";
    private String vimoAuthenKey = "2345678901";
    private HubConnection conn;
    private HubProxy proxy;
    public boolean running = true;
    static final ObjectMapper mapper = new ObjectMapper();
    static final DozerBeanMapper dmapper = new DozerBeanMapper();

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SignalRClient.class);

    public void connect() {
        // Create a new console logger
        final Logger slogger = new Logger() {

            @Override
            public void log(String message, LogLevel level) {
                //logger.info(message);
            }
        };       
        // Connect to the server
        conn = new HubConnection(getUrl(), queryString(), true, slogger);
        // Create the hub proxy
        proxy = conn.createHubProxy(getHubProxy());

        logger.info("conn = "+conn);
        logger.info("proxy = "+proxy);
        
        conn.error(new ErrorCallback() {
            @Override
            public void onError(Throwable error) {
                logger.info(error.getMessage());
            }
        });

        // Subscribe to the connected event
        conn.connected(new Runnable() {
            @Override
            public void run() {
                logger.info("CONNECTED");
            }
        });

        conn.reconnected(new Runnable() {
            @Override
            public void run() {
                logger.info("RECONNECTED");
            }
        });

        // Subscribe to the closed event
        conn.closed(new Runnable() {
            @Override
            public void run() {
                logger.info("DISCONNECTED");
                start();
//                new Timer(false).schedule(new TimerTask() {
//                    @Override
//                    public void run() {
//                        start(slogger);
//                    }
//                }, 5000);
            }
        });

        conn.stateChanged(new StateChangedCallback() {
            @Override
            public void stateChanged(ConnectionState state, ConnectionState newState) {
                if (null != newState) {
                    switch (newState) {
                        case Connected:
                            logger.info("STATE CHANGED-CONNECTED!");
                            break;
                        case Reconnecting:
                            logger.info("STATE CHANGED-RECONNECTING!");
                            break;
                        case Disconnected:
                            logger.info("STATE CHANGED-DISCONNECTED!");
                            break;
                        default:
                            break;
                    }
                }
            }
        });

        // Start the connection
        start();

        proxy.subscribe(new Object() {
            public void Shutdown() {
                try {
                    conn.stop();
                } finally {
                    running = false;
                }
            }

            public void Close() {
                try {
                    conn.stop();
                } finally {
                    running = false;
                }
            }

            @SuppressWarnings("unused")
            public void Response(String clientId, String requestId, String rqObject) {
                logger.info("REQUEST FROM VCB =" + rqObject);
                processResponse(clientId, requestId, rqObject);
            }
        });
    }

    public Response processRequest(String msgType, NVDataRequest req) throws Exception {
        logger.info("processRequest conn = "+conn);
        if (!conn.getState().equals(ConnectionState.Connected)) {
            throw new Exception("SignalR connection is not connected");
        }
        req.setPartnerId(getClientId());
        Response resp = new Response();
        String data;
        try {
            data = Security.encrypt(mapper.writeValueAsString(req));
            String signature = Security.sign(msgType + data);
            String vimoReq = mapper.writeValueAsString(new Request(msgType, data, signature));
            String requestId = getClientId() + System.currentTimeMillis();
            logger.info("Status = " + conn.getState());
            String result = proxy.invoke(String.class, "Request", getClientId(), requestId, vimoReq).get();
            logger.info("RESPONSE = " + result);
            if (result != null) {
                resp = mapper.readValue(result, Response.class);
                if (resp.getData() != null) {
                    resp.setData(Security.decrypt(resp.getData().toString()));
                    VcbDataResponse dataResp = mapper.readValue(resp.getData().toString(), VcbDataResponse.class);
                    String transData = dataResp.getData();
                    if (transData != null && !transData.isEmpty()) {
                        VcbTransDataResponse transDataResp = mapper.readValue(transData, VcbTransDataResponse.class);
                        resp.setVcbTransDataResp(transDataResp);
                    }
                    resp.setCode(dataResp.getCode());
                    resp.setMessage(dataResp.getMessage());
                    resp.setDesc(dataResp.getDesc());
                }
            }
            logger.info("DATA = " + resp.getData());
            return resp;
        } catch (InterruptedException | ExecutionException | IOException ex) {
            logger.info("processRequest " + ex.getMessage());
            throw ex;
        }
    }

    private void processResponse(String clientId, String requestId, String vcbMessage) {
        Response resp = new Response();
        NVCoreResponse vimoResp;
        Request req;
        try {
            req = mapper.readValue(vcbMessage, Request.class);
            VcbDataRequest vcbReq = mapper.readValue(Security.decrypt(req.getData()), VcbDataRequest.class);
            NVCoreRequest vimoReq = dmapper.map(vcbReq, NVCoreRequest.class);
            String fnc = Constant.FUNC_MAP.get(req.getMessageType());
//            if (fnc.equalsIgnoreCase("Active")) {
//                if (vcbReq.getCustomerId().contains("948123123")) {
//                    return;
//                }
//            }
            vimoResp = callVimo(vimoReq, fnc);
//            if (fnc.equalsIgnoreCase("Active")) {
//                if (vcbReq.getCustomerId().contains("948222333")) {
//                    return;
//                }
//            }
            if (vimoResp != null) {
                resp.setCode(vimoResp.getErrorCode());
                resp.setDesc(vimoResp.getMessage());
                resp.setMessage(vimoResp.getMessage());
                resp.setData(mapper.writeValueAsString(vimoResp.getData()));
            }
        } catch (Exception ex) {
            logger.info("processResponse " + ex.getMessage());
            resp.setCode(Constant.ERROR_SYSTEM);
            resp.setDesc("Loi exception");
        }
        sendResponse(clientId, requestId, resp);
    }

    public void sendResponse(String clientId, String requestId, Response resp) {
        Request rep = new Request();
        try {
            rep.setMessageType("");
            rep.setData(Security.encrypt(mapper.writeValueAsString(resp)));
            rep.setSignature(Security.sign(rep.getData()));
            String vimoResp = mapper.writeValueAsString(rep);
            logger.info("RETURN TO VCB " + vimoResp);
            proxy.invoke("Response", clientId, requestId, vimoResp);
        } catch (Exception ex) {
            logger.info(ex.getMessage());
        }
    }

    public void start() {
        conn.start()
                .done(new Action<Void>() {
                    @Override
                    public void run(Void obj) throws Exception {
                        logger.info("Done Connecting!");
                    }
                });
    }

    public void ping() {
        try {
            if (proxy != null) {
                String ping = proxy.invoke(String.class, "Ping", getClientId()).get();
                logger.info("PING " + ping);
                if (conn.getState() == ConnectionState.Disconnected) {
                    start();
//                    conn.start()
//                            .done(new Action<Void>() {
//                                @Override
//                                public void run(Void obj) throws Exception {
//                                    logger.info("Done Re Connecting!");
//                                }
//                            });
                }
            }
        } catch (InterruptedException ex) {
            logger.info("InterruptedException " + ex.getMessage());
        } catch (ExecutionException ex) {
            logger.info("ExecutionException " + ex.getMessage());
        }
    }

    private String queryString() {
        StringBuilder sb = new StringBuilder();
        sb.append("id=").append(getClientId()).append("&");
        sb.append("passcode=").append(getPassCode()).append("&");
        sb.append("clientversion=").append(CLIENT_VERSION);
        return sb.toString();
    }

    private NVCoreResponse callVimo(NVCoreRequest req, String fnc) throws Exception {
        Map<String, Object> map = new HashMap<>();
        String data = PGSecurity.encrypt3DES(mapper.writeValueAsString(req), getVimoAuthenKey());
        map.put("func", fnc);
        map.put("params", data);
        map.put("checksum", PGSecurity.md5(fnc, data));
        logger.info("Call VIMO PHP " + map.toString());
        String result = HttpUtil.send(getVimoUrl(), map, getVimoUrlUserPass());
        logger.info("response from  VIMO PHP " + result);
        NVCoreResponse resp = mapper.readValue(result, new TypeReference<NVCoreResponse>() {
        });
        return resp;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHubProxy() {
        return hubProxy;
    }

    public void setHubProxy(String hubProxy) {
        this.hubProxy = hubProxy;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPassCode() {
        return passCode;
    }

    public void setPassCode(String passCode) {
        this.passCode = passCode;
    }

    public String getVimoUrl() {
        return vimoUrl;
    }

    public void setVimoUrl(String vimoUrl) {
        this.vimoUrl = vimoUrl;
    }

    public String getVimoUrlUserPass() {
        return vimoUrlUserPass;
    }

    public void setVimoUrlUserPass(String vimoUrlUserPass) {
        this.vimoUrlUserPass = vimoUrlUserPass;
    }

    public String getVimoAuthenKey() {
        return vimoAuthenKey;
    }

    public void setVimoAuthenKey(String vimoAuthenKey) {
        this.vimoAuthenKey = vimoAuthenKey;
    }

//    public Response test(Request req) {
//        Response resp = new Response();
//        VimoCoreResponse vimoResp;
//        try {
//            VcbDataRequest vcbReq = mapper.readValue(Security.decrypt(req.getData()), VcbDataRequest.class);
//            VimoCoreRequest vimoReq = dmapper.map(vcbReq, VimoCoreRequest.class);
//            String fnc = Constant.FUNC_MAP.get(req.getMessageType());
//            vimoResp = callVimo(vimoReq, fnc);
//            if (vimoResp != null) {
//                resp = dmapper.map(vimoResp, Response.class);
////                if (vimoResp.getData() != null) {
////                    resp.setData(vimoResp.getData());
////                }
//            }
//        } catch (Exception ex) {
//            logger.info("processResponse " + ex.getMessage());
//            resp.setCode(Constant.ERROR_SYSTEM);
//            resp.setDesc("Loi exception");
//        }
//        return resp;
//    }
    public static void main(String[] args) throws IOException, Exception {
        //  String request = "{\"MessageType\":\"check_active\",\"Data\":\"uIP44UqoBJhc8f4MJIIghpnixynFuyFVmB0P0c/sqLTM8l3rn9J1tVyzzRzdNNEbGaFYPAMkbWTVhvZ/2vdZXTDpg1rfgYBY1KppNsIgv5CyGxO+3aNI1DcjOwmZNBEMN97LGxy0W5BgGyyf3nrqTg==\",\"Signature\":\"mOtqsPlPlsq62rWPK2dzD08eJj3MCL4EUz1ZsKVwcOFybtelAQJmZOm3uoQb6W/U6RNehjrFoDCckDo4p2xd0r3tnkyaQORi2TQDsmVZr9g71ow9UXHVHSLrBiiV0TruuCS52q28txDmS4HlklHCPWjhSMRKE2EkCGMUiVzcl55n2+m1gKoA+cK6gUJOs+d2PF5r38p7i1qufT3lGOlUi22vLzQZbK5UKSwG5YZiMLfxuJcQb5ER5jBCEklfIq/UXoP2kGf+MNnW8FSlvdg9lsLlrPJze2bQMR54Aa0VWt7HS2l6VKQ73beHmWu1VoJc9uhoEx1vjqooYvlwKLQD3A==\"}";
        SignalRClient client = new SignalRClient();
//         Response resp = new Response();
//         resp.setCode("1");
//         resp.setData("tesst");
//         resp.setMessage("22222");
//         resp.setDesc("11111");
//         client.sendResponse("1","2",resp);
//        Request req = new Request();
//        req.setMessageType("check_active");
//        req.setData("uIP44UqoBJhc8f4MJIIghoS11XXEfyKqTtOqJUzO+aQXQzquPIattie5Sgc6ivtD84evIsjPsvL7mA1K/eauwOTRYURklrqwaxkO8t4YNL4ghgUk0MixnCXRofY2v7iXyaVAbbIz3nEjz4nfwPVIdg==");
//        client.test(req);
        NVCoreRequest req = new NVCoreRequest();
        req.setUserId("0979586233");
        req.setVcbTransId("VCB" + System.currentTimeMillis());
        req.setAmount(100000D);
        String fnc = "GetInfo";
         NVCoreResponse vimoResp = client.callVimo(req, fnc);
        //System.out.print(mapper.writeValueAsString(client.callVimo(req, fnc)));
        System.out.print(mapper.writeValueAsString(vimoResp.getData()));
        // client.processResponse(null, null, request);
//        ObjectMapper mapper = new ObjectMapper();
//        String str = "{\"error_code\":\"1103\",\"message\":\"T\\u00e0i kho\\u1ea3n kh\\u00f4ng h\\u1ee3p l\\u1ec7 ho\\u1eb7c kh\\u00f4ng t\\u1ed3n t\\u1ea1i\",\"data\":\"\"}";
//        VimoCoreResponse resp = mapper.readValue(str, VimoCoreResponse.class);
    }

}
