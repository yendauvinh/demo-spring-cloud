/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.vcb.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.dozer.Mapping;

/**
 *
 * @author nnes
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NVCoreResponse {
    private String errorCode;
    private String message;
    private NVDataResponse data;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public NVDataResponse getData() {
        return data;
    }

    public void setData(NVDataResponse data) {
        this.data = data;
    }

}
