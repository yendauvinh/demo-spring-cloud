/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.vcb.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.dozer.Mapping;

/**
 *
 * @author nnes
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VcbDataRequest {

    private String vcbTrans;
    private String customerId;
    private String transDatetime;
    private String vcbId;
    private Double money;

    public String getVcbTrans() {
        return vcbTrans;
    }

    public void setVcbTrans(String vcbTrans) {
        this.vcbTrans = vcbTrans;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTransDatetime() {
        return transDatetime;
    }

    public void setTransDatetime(String transDatetime) {
        this.transDatetime = transDatetime;
    }

    public String getVcbId() {
        return vcbId;
    }

    public void setVcbId(String vcbId) {
        this.vcbId = vcbId;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "VcbDataRequest{" + "vcbTrans=" + vcbTrans + ", customerId=" + customerId + ", transDatetime=" + transDatetime + ", vcbId=" + vcbId + ", money=" + money + '}';
    }
}
