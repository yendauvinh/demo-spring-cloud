/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.vcb.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author nnes
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VimoDataRequest {

    private String PartnerId;
    private String UserId;
    private String TxnCode = "8000";
    private Double TxnAmount;
    private String TxnCurrency ="VND";
    private String TxnId;
    private String TxnDesc;
    private String TxnStatus = "0";

    public String getPartnerId() {
        return PartnerId;
    }

    public void setPartnerId(String PartnerId) {
        this.PartnerId = PartnerId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public String getTxnCode() {
        return TxnCode;
    }

    public void setTxnCode(String TxnCode) {
        this.TxnCode = TxnCode;
    }

    public Double getTxnAmount() {
        return TxnAmount;
    }

    public void setTxnAmount(Double TxnAmount) {
        this.TxnAmount = TxnAmount;
    }

    public String getTxnCurrency() {
        return TxnCurrency;
    }

    public void setTxnCurrency(String TxnCurrency) {
        this.TxnCurrency = TxnCurrency;
    }

    public String getTxnId() {
        return TxnId;
    }

    public void setTxnId(String TxnId) {
        this.TxnId = TxnId;
    }

    public String getTxnDesc() {
        return TxnDesc;
    }

    public void setTxnDesc(String TxnDesc) {
        this.TxnDesc = TxnDesc;
    }

    public String getTxnStatus() {
        return TxnStatus;
    }

    public void setTxnStatus(String TxnStatus) {
        this.TxnStatus = TxnStatus;
    }

}
