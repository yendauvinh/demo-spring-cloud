/**
 * 
 */
package nv.gateway.vcb.nvconnectvcb;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author YENDV
 *
 */
@Configuration
@EnableSwagger2
public class SwagerAppilication {
	@Value("${spring.application.name}")
    private String appName;
	
	@Bean
	public Docket api(){
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName(appName)
				.select()
				.apis(RequestHandlerSelectors.basePackage("nv.gateway.vcb.controller"))
				.paths(PathSelectors.any())
				.build();
	}
}
