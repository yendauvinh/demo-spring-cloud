/**
 * 
 */
package nv.gateway.vcb.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import nv.gateway.vcb.entity.PaymentChannelDirect;

/**
 * @author NV-Dev
 *
 */

public interface PaymentChannelDirectRepository 
//extends JpaRepository<PaymentChannelDirect, Long> 
{
	/*@Transactional
	@Modifying
	@Query("SELECT o FROM PaymentChannelDirect WHERE o.paymentChannel.paymentChannelCode = :paymentChannelCode")*/
	public PaymentChannelDirect getPmCnDrByPaymentChannelCode(String paymentChannelCode);
}
