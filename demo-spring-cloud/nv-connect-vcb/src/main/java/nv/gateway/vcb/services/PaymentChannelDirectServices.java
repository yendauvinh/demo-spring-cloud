/**
 * 
 */
package nv.gateway.vcb.services;

import nv.gateway.vcb.entity.PaymentChannelDirect;

/**
 * @author NV-Dev
 *
 */
public interface PaymentChannelDirectServices {
	PaymentChannelDirect getInfomationPmCnDr();
}
