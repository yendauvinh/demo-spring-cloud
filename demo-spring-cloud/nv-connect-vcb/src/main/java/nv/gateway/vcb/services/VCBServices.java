/**
 * 
 */
package nv.gateway.vcb.services;

import nv.gateway.vcb.entity.VimoDataRequest;
import nv.gateway.vcb.entity.VimoDataResponse;

/**
 * @author NV-Dev
 *
 */
public interface VCBServices {
	VimoDataResponse paymentWithToken(VimoDataRequest req);
}
