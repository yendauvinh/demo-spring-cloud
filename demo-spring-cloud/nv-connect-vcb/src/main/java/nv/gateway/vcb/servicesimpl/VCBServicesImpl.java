/**
 * 
 */
package nv.gateway.vcb.servicesimpl;

import org.apache.log4j.Logger;
import gateway.request.LinkedRequestObject;
import gateway.request.PaymentRequestObject;
import gateway.request.QueryRequestObject;
import gateway.request.TakeoutWalletRequestObject;
import gateway.response.ResponseObject;
import gateway.services.Gateway;
import nv.gateway.vcb.constant.SignalRClient;
import nv.gateway.vcb.entity.NVDataRequest;
import nv.gateway.vcb.entity.Response;

/**
 * @author NV-Dev
 *
 */
//@Service
public class VCBServicesImpl extends Gateway {
	
	private static final Logger logger = Logger.getLogger(VCBServicesImpl.class);
	private SignalRClient client;
	private final String doubleRegex = "^[-+]?\\d+(\\.{0,1}(\\d+?))?$";

	@Override
	public ResponseObject cashIn(PaymentRequestObject req ) {
		Response response = null;
		try {
			createClient();
			logger.info("client = " + client);
			NVDataRequest nvReq = parseRequest(req);
			response = client.processRequest("cash_in" , nvReq);
		} catch (Exception e) {
			logger.error(e);
		}
		return parseResponseObj(response);
	}

	@Override
	public ResponseObject cashOut(TakeoutWalletRequestObject req) {
		Response response = null;
		try {
			createClient();
			logger.info("client = " + client);
			NVDataRequest nvReq = parseRequest(req);
			response = client.processRequest("cash_out" , nvReq);
		} catch (Exception e) {
			logger.error(e);
		}
		return parseResponseObj(response);
	}
	
	@Override
	public ResponseObject checkActiveStatus(LinkedRequestObject req) {
		Response response = null;
		try {
			createClient();
			logger.info("client = " + client);
			NVDataRequest nvReq = parseRequest(req);
			response = client.processRequest("cash_out" , nvReq);
		} catch (Exception e) {
			logger.error(e);
		}
		return parseResponseObj(response);
	}
	
	@Override
	public ResponseObject query(QueryRequestObject req) {
		Response response = null;
		try {
			createClient();
			logger.info("client = " + client);
			NVDataRequest nvReq = parseRequest(req);
			response = client.processRequest("cash_out" , nvReq);
		} catch (Exception e) {
			logger.error(e);
		}
		return parseResponseObj(response);
	}
	
	private void createClient() {
        if (client == null) {
            client = new SignalRClient();
            /*PaymentChannel pc = getPaymentChannel();
            PaymentAccount pa = getPaymentAccount();
            client.setUrl(pc.getUrl());
            client.setHubProxy(pc.getHost());
            client.setClientId(pa.getUsername());
            client.setPassCode(pa.getPassword());
            client.setVimoUrl(pa.getInfo());*/
            client.connect();
        } 
    }
	
	private NVDataRequest parseRequest(PaymentRequestObject req){
		NVDataRequest nvReq = new NVDataRequest();
		nvReq.setUserId(req.getToken()); // when channel payment is VCB, user id is token.
		if(req.getAmount() != null && req.getAmount().matches(doubleRegex))
			nvReq.setTxnAmount(Double.parseDouble(req.getAmount()));
		nvReq.setTxnCurrency(req.getCurrencyCode());
		nvReq.setTxnId(req.getRequestId());
		nvReq.setTxnDesc(req.getDesription());
		return nvReq;
	}
	
	private NVDataRequest parseRequest(TakeoutWalletRequestObject req){
		NVDataRequest nvReq = new NVDataRequest();
		nvReq.setUserId(req.getToken()); // when channel payment is VCB, user id is token.
		if(req.getAmount() != null && req.getAmount().matches(doubleRegex))
			nvReq.setTxnAmount(Double.parseDouble(req.getAmount()));
		nvReq.setTxnCurrency(req.getCurrencyCode());
		nvReq.setTxnId(req.getRequestId());
		nvReq.setTxnDesc(req.getDescription());
		return nvReq;
	}
	
	private NVDataRequest parseRequest(LinkedRequestObject req){
		NVDataRequest nvReq = new NVDataRequest();
		nvReq.setUserId(req.getCustomerId()); // when channel payment is VCB, user id is token.
		nvReq.setTxnId(req.getCustomerId());
		/*if(req.getAmount() != null && req.getAmount().matches(doubleRegex))
			nvReq.setTxnAmount(Double.parseDouble(req.getAmount()));
		nvReq.setTxnCurrency(req.getCurrencyCode());
		nvReq.setTxnId(req.getRequestId());
		nvReq.setTxnDesc(req.getDescription());*/
		return nvReq;
	}
	
	private NVDataRequest parseRequest(QueryRequestObject req){
		NVDataRequest nvReq = new NVDataRequest();
		nvReq.setUserId(req.getCustomerId()); // when channel payment is VCB, user id is token.
		nvReq.setTxnId(req.getCustomerId());
		/*if(req.getAmount() != null && req.getAmount().matches(doubleRegex))
			nvReq.setTxnAmount(Double.parseDouble(req.getAmount()));
		nvReq.setTxnCurrency(req.getCurrencyCode());
		nvReq.setTxnId(req.getRequestId());
		nvReq.setTxnDesc(req.getDescription());*/
		return nvReq;
	}
	
	private ResponseObject parseResponseObj(Response res){
		ResponseObject resObj = new ResponseObject();
		resObj.setCode(res.getCode());
		resObj.setData(res.getVcbTransDataResp());
		if(res.getVcbTransDataResp() != null) resObj.setIsSuccess(true);
		else resObj.setIsSuccess(false);
		resObj.setMessage(res.getDesc());
		return resObj;
	}

	
}
