/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nv.gateway.vcb.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.utils.Base64;

/**
 *
 * @author nnes
 */
public class PGSecurity {

    private static final Logger logger = LogManager.getLogger(PGSecurity.class);

    public static String md5(String... input) throws NoSuchAlgorithmException {
        StringBuilder sb = new StringBuilder();
        for(String str: input){
            sb.append(str);
        }
        MessageDigest instance = MessageDigest.getInstance("MD5");
        byte[] byteData = instance.digest(sb.toString().getBytes());
        // convert the byte to hex format
        String hexString = DatatypeConverter.printHexBinary(byteData);
        return hexString.toLowerCase();
    }

    public static String encryptAES(String key, String value) {
        try {
            //IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, generateRandomIv());

            byte[] encrypted = cipher.doFinal(value.getBytes());
            return Base64.encode(encrypted);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
            logger.info(ex.getMessage());
        }

        return null;
    }

    public static String decryptAES(String key, String encrypted) {
        try {
            //IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, generateRandomIv());
            byte[] original = cipher.doFinal(Base64.decode(encrypted.getBytes("UTF-8")));
            return new String(original);
        } catch (Base64DecodingException | UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
            logger.info(ex.getMessage());
        }

        return null;
    }

    public static String encrypt3DES(String input, String key) throws Exception {
        byte[] arrayBytes = getValidKey(key);
        KeySpec ks = new DESedeKeySpec(arrayBytes);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");
        Cipher cipher = Cipher.getInstance("DESede");
        SecretKey seckey = skf.generateSecret(ks);
        //
        cipher.init(Cipher.ENCRYPT_MODE, seckey);
        byte[] plainByte = input.getBytes();
        byte[] encryptedByte = cipher.doFinal(plainByte);
        return Base64.encode(encryptedByte);

    }

    public static String decrypt3DES(String encrypted, String key) throws Exception {
        byte[] arrayBytes = getValidKey(key);
        KeySpec ks = new DESedeKeySpec(arrayBytes);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");
        Cipher cipher = Cipher.getInstance("DESede");
        SecretKey seckey = skf.generateSecret(ks);
        cipher.init(Cipher.DECRYPT_MODE, seckey);
        byte[] encryptByte = Base64.decode(encrypted);
        byte[] plainByte = cipher.doFinal(encryptByte);
        return new String(plainByte);
    }

    private static byte[] getValidKey(String key) {
        try {
            String sTemp = md5(key).substring(0, 24).toLowerCase();
            return sTemp.getBytes();
        } catch (Exception e) {
        }
        return null;
    }

    private static IvParameterSpec generateRandomIv() {
        byte[] iv = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(iv);
        return new IvParameterSpec(iv);
    }
}
