package nv.gateway.vcb.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PGValidator {

    @Autowired
    private Validator baseValidator;

    public <T> Map<String, String> validate(T object) {
        Map<String, String> errors = new HashMap<>();
        if (object == null) {
            return errors;
        }
        Set<ConstraintViolation<T>> constraints = baseValidator.validate(object);
        for (ConstraintViolation<T> constraint : constraints) {
            errors.put(constraint.getPropertyPath().toString(), constraint.getMessage());
        }
        return errors;
    }

    public <T> String validateParam(T object) {
        if (object == null) {
            return null;
        }
        Set<ConstraintViolation<T>> constraints = baseValidator.validate(object);
        for (ConstraintViolation<T> constraint : constraints) {
            return constraint.getMessage();
        }
        return null;
    }

    public <T> void validate1(T object) throws Exception {
        if (object == null) {
            return;
        }
        Set<ConstraintViolation<T>> constraints = baseValidator.validate(object);
        for (ConstraintViolation<T> constraint : constraints) {
            throw new Exception(constraint.getMessage());
        }
    }

}
