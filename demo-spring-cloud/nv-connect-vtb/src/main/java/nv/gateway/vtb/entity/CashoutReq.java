package nv.gateway.vtb.entity;

@SuppressWarnings("serial")
public class CashoutReq extends AbstractRequest {

	private String token;
	private String tokenIssueDate;
	private String amount;
	private String currencyCode;

	private String benName;
	private String benAcctNo;
	private String benIDNo;
	private String benAddInfo;

	private String remark;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTokenIssueDate() {
		return tokenIssueDate;
	}

	public void setTokenIssueDate(String tokenIssueDate) {
		this.tokenIssueDate = tokenIssueDate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getBenName() {
		return benName;
	}

	public void setBenName(String benName) {
		this.benName = benName;
	}

	public String getBenAcctNo() {
		return benAcctNo;
	}

	public void setBenAcctNo(String benAcctNo) {
		this.benAcctNo = benAcctNo;
	}

	public String getBenIDNo() {
		return benIDNo;
	}

	public void setBenIDNo(String benIDNo) {
		this.benIDNo = benIDNo;
	}

	public String getBenAddInfo() {
		return benAddInfo;
	}

	public void setBenAddInfo(String benAddInfo) {
		this.benAddInfo = benAddInfo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
