package nv.gateway.vtb.entity;

@SuppressWarnings("serial")
public class InquiryTrxReq extends AbstractRequest {

	private String queryTransactionId;
	private String queryType;

	public String getQueryTransactionId() {
		return queryTransactionId;
	}

	public void setQueryTransactionId(String queryTransactionId) {
		this.queryTransactionId = queryTransactionId;
	}

	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

}
