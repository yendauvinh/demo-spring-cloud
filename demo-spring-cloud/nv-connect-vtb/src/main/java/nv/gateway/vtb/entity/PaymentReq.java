package nv.gateway.vtb.entity;

@SuppressWarnings("serial")
public class PaymentReq extends AbstractRequest {

	private String token;
	private String tokenIssueDate;
	private String amount;
	private String currencyCode;

	private String payMethod;
	private String goodsType;
	private String billNo;
	private String remark;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTokenIssueDate() {
		return tokenIssueDate;
	}

	public void setTokenIssueDate(String tokenIssueDate) {
		this.tokenIssueDate = tokenIssueDate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
