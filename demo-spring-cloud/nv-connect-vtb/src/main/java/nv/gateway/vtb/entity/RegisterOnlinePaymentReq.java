package nv.gateway.vtb.entity;

@SuppressWarnings("serial")
public class RegisterOnlinePaymentReq extends AbstractRequest {

	private String cardNumber;
	private String cardIssueDate;
	private String cardHolderName;

	private String providerCustId;

	private String custIDNo;
	private String custIDIssueDate;
	private String custIDIssueBy;
	private String custPhoneNo;

	private String custGender;
	private String custBirthday;

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardIssueDate() {
		return cardIssueDate;
	}

	public void setCardIssueDate(String cardIssueDate) {
		this.cardIssueDate = cardIssueDate;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public String getProviderCustId() {
		return providerCustId;
	}

	public void setProviderCustId(String providerCustId) {
		this.providerCustId = providerCustId;
	}

	public String getCustIDNo() {
		return custIDNo;
	}

	public void setCustIDNo(String custIDNo) {
		this.custIDNo = custIDNo;
	}

	public String getCustIDIssueDate() {
		return custIDIssueDate;
	}

	public void setCustIDIssueDate(String custIDIssueDate) {
		this.custIDIssueDate = custIDIssueDate;
	}

	public String getCustIDIssueBy() {
		return custIDIssueBy;
	}

	public void setCustIDIssueBy(String custIDIssueBy) {
		this.custIDIssueBy = custIDIssueBy;
	}

	public String getCustPhoneNo() {
		return custPhoneNo;
	}

	public void setCustPhoneNo(String custPhoneNo) {
		this.custPhoneNo = custPhoneNo;
	}

	public String getCustGender() {
		return custGender;
	}

	public void setCustGender(String custGender) {
		this.custGender = custGender;
	}

	public String getCustBirthday() {
		return custBirthday;
	}

	public void setCustBirthday(String custBirthday) {
		this.custBirthday = custBirthday;
	}

}
