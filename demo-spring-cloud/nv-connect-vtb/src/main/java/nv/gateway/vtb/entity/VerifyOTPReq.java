package nv.gateway.vtb.entity;

@SuppressWarnings("serial")
public class VerifyOTPReq extends AbstractRequest {

	private String otp;
	private String verifyTransactionId;
	private String verifyBy;

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getVerifyTransactionId() {
		return verifyTransactionId;
	}

	public void setVerifyTransactionId(String verifyTransactionId) {
		this.verifyTransactionId = verifyTransactionId;
	}

	public String getVerifyBy() {
		return verifyBy;
	}

	public void setVerifyBy(String verifyBy) {
		this.verifyBy = verifyBy;
	}

}
