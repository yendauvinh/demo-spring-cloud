/**
 * 
 */
package nv.gateway.vtb.servicesimpl;

import gateway.request.DestroyProfileRequestObject;
import gateway.request.LinkedRequestObject;
import gateway.request.OtpRequestObject;
import gateway.request.PaymentRequestObject;
import gateway.request.QueryRequestObject;
import gateway.request.TakeoutWalletRequestObject;
import gateway.response.ResponseObject;
import gateway.services.Gateway;

/**
 * @author NV-Dev
 *
 */
public class VTBServicesImpl extends Gateway {
	@Override
	public ResponseObject destroyProfile(DestroyProfileRequestObject request){
		return null;
	}
	
	@Override
	public ResponseObject linkCard(LinkedRequestObject request) {
		return null;
	}

	@Override
	public ResponseObject getOtp(OtpRequestObject request){
		return null;
	}

	@Override
	public ResponseObject cashIn(PaymentRequestObject request) {
		return null;
	}

	@Override
	public ResponseObject cashOut(TakeoutWalletRequestObject request) {
		return null;
	}

	@Override
	public ResponseObject checkActiveStatus(LinkedRequestObject req) {
		return null;
	}

	@Override
	public ResponseObject query(QueryRequestObject req) {
		return null;
	}
}
