/**
 * 
 */
package nv.gateway.monitor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nv.gateway.monitor.service.ClientService;

/**
 * @author NV-Dev
 *
 */
@RestController
public class ClientController {
	@Autowired
	private ClientService clientService;
	
	@RequestMapping(value = "/checkConnect" , method = RequestMethod.GET)
	public String checkConnect(@RequestParam("urlDetail") String urlDetail){
		return clientService.checkConnection(urlDetail);
	}
}
