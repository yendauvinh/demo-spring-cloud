package nv.gateway.monitor.nvgatewaymonitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableCircuitBreaker
@EnableDiscoveryClient
@ComponentScan({"nv.gateway.monitor.service" , "nv.gateway.monitor.controller"})
public class NvGatewayMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(NvGatewayMonitorApplication.class, args);
	}
}
