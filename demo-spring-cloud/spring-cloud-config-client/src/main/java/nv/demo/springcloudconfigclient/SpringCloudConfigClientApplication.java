package nv.demo.springcloudconfigclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@RefreshScope
@Configuration
public class SpringCloudConfigClientApplication {
	@Value("${test.property}")
    private String testProperty;

    @Value("${test.local.property}")
    private String localTestProperty;
    
    /*@Autowired
    private PropertyConfiguration propertyConfiguration;*/

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudConfigClientApplication.class, args);
		System.out.println("ok");
	}
	
	@RequestMapping(value = "/" , method = RequestMethod.GET)
	public String test(){
		return testProperty + " = " + localTestProperty;
	}
}
